import React, { useRef } from "react";
import { useRouter } from 'next/router'
import Image from "next/image";

type Props = {};

const Header = (props: Props) => {
  const router = useRouter()

  const select = useRef<HTMLSelectElement>(null)

  const sort = () => {
    router.push({
      pathname: '/',
      query: { sort: select?.current?.value }
    })
  }

  return (
    <header className="bg-[#04adef] fixed h-[60px] w-[100%] box-border z-50" data-testid="header">
      <div className="w-[100%] max-w-[992px] mx-auto h-[100%] flex items-center justify-between flex-row p-3">
        <div className="flex items-center flex-row">
          <Image
            src="https://assets.kitabisa.cc/images/logos/logogram__ktbs_white.png"
            width={33}
            height={33}
            alt="Logo"
          />
          <h1 className="ml-3 text-white font-sans lg:text-[1.5rem] text-lg">
            Kitabisa
          </h1>
        </div>
        <div className="flex items-center flex-row">
          <select 
            data-testid="select-options"
            ref={select}
            onChange={sort}
            defaultValue={router?.query?.sort || "donation-goal-asc"}
            className="lg:text-md text-sm text-right bg-transparent border-0 p-2 active:border-0 focus:outline-none focus:border-0 text-white">
            <option value="donation-goal-asc" >Sorting by donation goal (A-Z)</option>
            <option value="donation-goal-desc" >Sorting by donation goal (Z-A)</option>
            <option value="days-left-asc" >Sorting by days left (A-Z)</option>
            <option value="days-left-desc" >Sorting by days left (Z-A)</option>
          </select>
        </div>
      </div>
    </header>
  );
};

export default Header;
