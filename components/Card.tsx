import React from "react";
import moment from 'moment'
import Image from "next/image";
import shallow from 'zustand/shallow'

import { useStore } from './../store/context';

import { ObjCampaign } from './../interfaces/I_Campaign'

type Props = {};

const Card = (props: Props) => {

  const { listCampaign } = useStore(
    (store: any) => ({ listCampaign: store.listCampaign }),
    shallow
  )

  const CalcDay = (timestamp: number): string => {
    const dateEx = new Date(moment(moment.unix(timestamp)).format("MM/DD/YYYY")).getTime();
    return new Intl.NumberFormat("en-US", { style: "decimal" }).format(Math.ceil((dateEx - new Date().getTime()) / (1000 * 3600 * 24)))
  }

  return (
    <div className="flex flex-wrap mt-2" data-testid="card">

      {listCampaign.map((val: ObjCampaign, i: number) =>
        <div className="w-[100%] lg:w-[325px] sm:w-[50%] px-2 mb-5" key={i}>
          <Image
            layout="responsive"
            src={val.image}
            width={325}
            height={180}
            placeholder="blur"
            blurDataURL={val.image}
            alt={val.title}
          />
          <h2 className="md:text-[1.2rem] md:font-medium text-md font-bold sm:text-ellipsis sm:h-[55px] sm:overflow-hidden mt-2">{val.title}</h2>
          <span className="h-[8px] w-[100%] block bg-gray-400 my-2 rounded-sm" >
            <span className="h-[8px] block bg-pink-500 rounded-sm" style={{ width: `${val.donation_percentage}%`, maxWidth: '100%' }} />
          </span>
          <div className="flex justify-between">
            <div>
              <h6 className="text-sm">Terkumpul</h6>
              <span className="font-bold text-base">{
                Intl.NumberFormat('id-ID', {
                  style: 'currency',
                  currency: 'IDR'
                }).format(val.donation_received)
              }</span>
            </div>
            <div>
              <h6 className="text-sm">Sisa Hari</h6>
              <span className="font-bold text-base">{CalcDay(val.expired)}</span> <b />
              <b />
            </div>
          </div>
        </div>
      )}

    </div>
  );
};

export default Card;
