
export interface Store {
  listCampaign: Array<ObjCampaign>;
  getListCampaign: (sort: string) => void;
}

export interface ObjCampaign {
  id: number;
  order: number;
  parent_project_id: number;
  campaigner: string;
  title: string;
  expired: number;
  image: string;
  days_remaining: number;
  donation_received: number;
  donation_target: number;
  campaigner_type: string;
  campaigner_badge: string;
  campaigner_is_verified: boolean;
  category_name: string;
  is_forever_running: boolean;
  is_open_goal: boolean;
  request_userdata: boolean;
  donation_percentage: number;
  short_url: string;
  is_featured: number;
  custom_fb_pixel: string;
}