/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['assets.kitabisa.cc', 'img.staging.kitabisa.cc', 'kitabisa-userupload-01.s3-ap-southeast-1.amazonaws.com'],
  },
}

module.exports = nextConfig
