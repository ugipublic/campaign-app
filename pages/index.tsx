import type { NextPage } from "next";
import Head from "next/head";

import { initializeStore } from '../store'

/* Import Components */
import Header from "./../components/Header";
import Card from "./../components/Card";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Campaign App</title>
        <meta name="description" content="Campaign App" />
        <link
          rel="shortcut icon"
          type="image/png"
          href="https://assets.kitabisa.cc/images/icons/meta/favicon.ico"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="https://assets.kitabisa.cc/images/icons/meta/favicon-16x16.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="https://assets.kitabisa.cc/images/icons/meta/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="https://assets.kitabisa.cc/images/icons/meta/android-chrome-192x192.png"
        />
      </Head>

      <Header />

      <div className="bg-white w-[100%] max-w-[992px] top-[60px] relative mx-auto p-2 box-border">
        <Card />
      </div>
    </>
  );
};

Home.getInitialProps = async ({ req, query }) => {
  const { sort } = query;
  const store = initializeStore()

  await store.getState().getListCampaign(sort || 'donation-goal-asc')

  return {
    dataStore: store.getState()
  };
};

export default Home;
