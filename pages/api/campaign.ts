import type { NextApiRequest, NextApiResponse } from "next";
import http from "../../utils/http";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    try {
        const { data } = await http.get("https://storage.googleapis.com/southern-waters-642.appspot.com/fe_code_challenge/campaign.json")
        res.status(200).json(data.data)
    } catch (err: any) {
        res.status(err.response.status).json({
            message: "Something wrong error",
            code: err.response.status,
        });
    }
}
