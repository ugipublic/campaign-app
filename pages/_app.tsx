import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { useHydrate } from './../store'

import { StoreProvider } from './../store/context'

function MyApp({ Component, pageProps }: AppProps) {
  const store = useHydrate(pageProps.dataStore)

  return (
    <StoreProvider store={store}>
      <Component {...pageProps} />
    </StoreProvider>
  )
}

export default MyApp
