import "@testing-library/jest-dom";
import { fireEvent, render, screen, renderHook } from "@testing-library/react";
import { RouterContext } from 'next/dist/shared/lib/router-context';
/* Interfaces */
import { ObjCampaign } from './../interfaces/I_Campaign';

/* Components */
import Header from "../components/Header";
import Card from "../components/Card";

/* Pages */
import Home from '../pages/index';

/* Store Zustand */
import { useHydrate } from '../store'
import { StoreProvider } from "../store/context";

import { createMockRouter } from './RouterMock';

interface Props {
    listCampaign: Array<ObjCampaign>
}

const customRender = (ui: React.ReactNode, { ...providerProps }: Props) => {
    const store = renderHook(() => useHydrate(providerProps))
    console.log(store)
    return render(
        <StoreProvider store={store}>{ui}</StoreProvider>
    );
};

describe("Render components", () => {
    // const listCampaign = [
    //         {
    //             "id": 94597,
    //             "order": 1,
    //             "parent_project_id": 0,
    //             "title": "#BisaBangkit Bersama Kitabisa",
    //             "expired": 2147483647,
    //             "image": "https://img.staging.kitabisa.cc/size/664x357/0f9a7205-79ef-49c9-a95a-49347fbd00a6.jpg",
    //             "days_remaining": 0,
    //             "donation_received": 178613497,
    //             "campaigner": "Kitabisa.com",
    //             "campaigner_type": "PERSONAL",
    //             "campaigner_badge": "https://assets.kitabisa.com/images/icon__verified-user.svg",
    //             "campaigner_is_verified": true,
    //             "category_name": "Bencana Alam",
    //             "is_forever_running": true,
    //             "is_open_goal": false,
    //             "request_userdata": false,
    //             "donation_target": 500000000,
    //             "donation_percentage": 0.357227,
    //             "short_url": "bisabangkit",
    //             "is_featured": 0,
    //             "custom_fb_pixel": ""
    //         },
    //     ]

    test("render header", () => {
        render(<Header />);
        expect(screen.getByTestId("header")).toBeInTheDocument();
    });

    // it("render card", () => {
    //     customRender(<Home />, { listCampaign });
    //     expect(screen.getByTestId("card")).toBeInTheDocument();
    // });
});

describe("Action", () => {
    const router = createMockRouter({
        pathname: '/',
    });

    beforeEach(() => {
        render(
            <RouterContext.Provider value={createMockRouter(router)}>
                <Header />
            </RouterContext.Provider>
        );
    });

    test("should default select 'Sorting by donation goal (A-Z)' ", () => {
        expect(screen.getByRole<HTMLOptionElement>('option', { name: 'Sorting by donation goal (A-Z)' }).selected).toBe(true)
    })
    test("should add query param sort when user select sorting", () => {
        fireEvent.change(screen.getByTestId("select-options"), { target: { value: 'days-left-asc' } })
        expect(screen.getByRole<HTMLOptionElement>('option', { name: 'Sorting by days left (A-Z)' }).selected).toBe(true)
        expect(router.push).toHaveBeenCalledWith(
            { "pathname": "/", "query": { "sort": "days-left-asc" } }
        );
    })
})