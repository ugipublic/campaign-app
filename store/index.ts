import { useMemo } from "react";
import create from "zustand";
import { devtools } from "zustand/middleware";
import http from "./../utils/http";

import { ObjCampaign } from "./../interfaces/I_Campaign";

/* Interfaces */
import { Store } from "./../interfaces/I_Campaign";

const initialState = {
    listCampaign: [],
};

let store: any;

const initStore = (preloadedState = initialState) => {
    return create<Store>()(
        devtools(
            (set) => ({
                ...initialState,
                ...preloadedState,
                getListCampaign: async (sort: string) => {
                    const { data } = await http.get("/api/campaign");

                    switch (sort) {
                        case "donation-goal-desc":
                            data.sort((a: ObjCampaign, b: ObjCampaign) =>
                                a.donation_target < b.donation_target ? 1 : -1
                            );
                            break;
                        case "days-left-asc":
                            data.sort((a: ObjCampaign, b: ObjCampaign) =>
                                a.expired > b.expired ? 1 : -1
                            );
                            break;
                        case "days-left-desc":
                            data.sort((a: ObjCampaign, b: ObjCampaign) =>
                                a.expired < b.expired ? 1 : -1
                            );
                            break;
                        default:
                            data.sort((a: ObjCampaign, b: ObjCampaign) =>
                                a.donation_target > b.donation_target ? 1 : -1
                            );
                    }

                    set((state) => ({
                        ...state,
                        listCampaign: data,
                    }));
                },
            })
        )
    );
};

export const initializeStore = (preloadedState?: any) => {
    let _store = store ?? initStore(preloadedState);

    // After navigating to a page with an initial Zustand state, merge that state
    // with the current state in the store, and create a new store
    if (preloadedState && store) {
        _store = initStore({
            ...store.getState(),
            ...preloadedState,
        });
        // Reset the current store
        store = undefined;
    }

    // For SSG and SSR always create a new store
    if (typeof window === "undefined") return _store;
    // Create the store once in the client
    if (!store) store = _store;

    return _store;
};

export function useHydrate(initialState: any) {
    const state =
        typeof initialState === "string"
            ? JSON.parse(initialState)
            : initialState;
    const store = initializeStore(state);
    return store;
}

export default initStore;
