import { createContext, useContext } from 'react'

interface I_StoreContext {
    store: any;
    children: any;
}

export const StoreContext = createContext<I_StoreContext | null>(null)

export const StoreProvider = ({ children, store }: I_StoreContext) => {
  return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
}

export const useStore = (selector: any, eqFn: any) => {
  const store: any = useContext(StoreContext)

  const values = store(selector, eqFn)

  return values
}
